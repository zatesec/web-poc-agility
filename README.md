# Docker Pets deploy para Google Cloud

Docker Pets é um projeto simples utilizado como exemplo, uma aplicação em python usando imagem alpine como base.

Source: https://github.com/docker-archive/docker-pets

## Objetivo

Este projeto envolve em desenvolver uma pipeline com o básico de vários aspectos como best pratices e segurança básica para Containers.

## Arquitetura da Pipeline

Quebrando nos itens abaixo:
	- test: Parte básica de testes da aplicação e segurança estática.
  	- build: Parte responsável por gerar o primeiro artefato, assim validando o build do container que pode ser utilizado para testes locais.
  	- trivy: Parte responsável por fazer testes de segurança no container, verificando se a imagem contem versões de pacote e sistema operacional com CVE
  	- build-latest: Build da Imagem que só será executada caso as anteriores tenham sido realizadas com sucesso e na branch homolog.
  	- deploy: Dividido em 2 ambientes, de acordo com a branch executada, main e homolog, realizando o deploy para google cloud ou qualquer ambiente de produção desejado.

## Preparação Inicial - Best Pratices

Organização e limpeza de arquivos e informações não utilizadas neste fork.

Pelo projeto ser relativamente antigo ( 4 anos ) a versão do alpine utilizada era 3.3, atualmente estamos na 3.12.2, neste caso foi realizado ou upgrade e validação da imagem Base por questões de segurança(Sempre é necessário garantir a funcionalidade).

Upgrade para 3.12.2 com sucesso.

Pequena alteração no projeto para realizar o display da branch e commit está ativo no ambiente (facilita o acompanhamento da versão em homolog/prod)

Recomendações/TO-DO: Migrar a aplicação para python 3, já que python 2 já expirou o suporte (01/01/2020).


## Customização Google Cloud.

Google Cloud App Engine precisa de um arquivo com app.yaml contendo algumas informações de acordo com o projeto,
neste caso como utilizamos uma imagem própria e não a fornecida pela google, iremos utilizar as informações abaixo:
```
runtime: custom
env: flex
```
Outro ponto importante é que o gcp app engine, espera que a porta a ser utilizada pela aplicação seja 8080.

Para realizarmos o processo no google cloud platform é importante configuramos algumas variáveis de ambiente de forma segura.
Neste caso estou utilizando as funções do GITLAB de Variáveis, desta forma não fica expostas a qualquer pessoa.
```
$DEPLOY_KEY_FILE_PRODUCTION
$PROJECT_ID_PRODUCTION
```
É importante sempre seguir a regra do mínimo privilegio possível, então recomenda-se liberar cada permissão e testar e ir liberando sob demanda com cuidado no usuário de deploy.

## Execução do container para testes
```
$ docker run -it -p 8080:8080 imagename
```
## to-do

adicionar nginx na frente com configurações, com base nas recomendações:
https://ssl-config.mozilla.org/#server=nginx&version=1.18&config=intermediate&openssl=1.1.1d&guideline=5.6
é necessario validar cada configuração e customizar 100% para o website a ser hospedado, validando seu funcionamento.
colocar security headers, exemplo:
````
add_header Feature-Policy "geolocation none;midi none;notifications none;push none;sync-xhr none;microphone none;camera none;magnetometer none;gyroscope none;speaker self;vibrate none;fullscreen self;payment none;";
````
Entre outras configurações, resultado desejável é semelhante ao abaixo:
Nota A ou A+
https://www.ssllabs.com/ssltest/analyze.html?d=wp.fiap.loatecs.com&latest
Nota A ou A+
https://securityheaders.com/?q=https%3A%2F%2Fwp.fiap.loatecs.com%2F&followRedirects=on

## Observaçoes:
Permissions-Policy no security headers foi adicionado recentemente, então ainda não é amplamente utilizado.
Este site é um ambiente criado com intuito de proteger da melhor forma um ambiente em WordPress no nivel de infraestrutura.
https://wp.fiap.loatecs.com/
HAproxy + nginx + wordpress latest.

## Considerações finais:

Foi interessante pegar um projeto antigo e atualizar ele na medida do possivel.

Infelizmente não tive o tempo suficiente para fazer tudo que tinha em mente, como multiplos deploy no azure aws e vagrant e ansible, mas é isso, infelizmente não ando praticando tanto cloud, na empresa atualmente foco maior é controle de frota, atualmente gerencio sozinho 100 VMS, 10 hypervisors (vmware e hyper-v), faço monitoramento de 500 equipamentos espalhado em todo pais e seus serviços customizados, então é devops e automação sobdemanda exclusiva e não generica.
