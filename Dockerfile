
FROM alpine:latest
## added apk update and upgrade to prevent vul like CVE-2020-1971
## manual install pip because by default alpine pip is for python3 , python2 end of life.
RUN apk update && apk upgrade && apk --no-cache add libpq python2-dev curl && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python2 get-pip.py && rm -rf get-pip.py && pip install flask==0.10.1 python-consul

ADD ./web/ /app

WORKDIR /app

HEALTHCHECK CMD curl --fail http://localhost:5000/health || exit 1

CMD python app.py & python admin.py
